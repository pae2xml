#!/usr/bin/perl


# Copyright (C) 2003 Rainer Typke
# Copyright (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
#pae2xml is licensed under the terms of the GNU General Public License Version
#2 as published by the <a href="http://www.fsf.org/" target="_top">Free Software Foundation</a>.
#This gives you legal permission to copy, distribute and/or modify <em>pae2xml</em> under
#certain conditions. Read
#the <a href="http://www.gnu.org/copyleft/gpl.html" target="_top">online version of the license</a>
#for more details. pae2xml is provided AS IS with NO WARRANTY OF ANY KIND,
#INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE.

our $script = "pae2xml.pl";
our $version = "1.0";

### Handle command-line options

# import module
use Getopt::Long;
use File::Basename;

our $format = 'rism';

sub print_help {
  print 'pae2xml.pl [OPTIONS...] FILE

  Converts the Plaine & Easie file FILE to MusicXML. If FILE is -, STDIN is used.

  Possible options:
    -h, --help          Prints out this help message
    --format=FORMAT, -f FORMAT
                        Interprets input file as FORMAT. Possible values are
                           rism, pae (default: rism)
    -v, -- version      Prints out version information
';
}
sub print_version {
  print "$script $version
  Copyright (C) 2003 Rainer Typke
  Copyright (C) 2010 Reinhold Kainhofer <reinhold\@kainhofer.com>
";
}
sub handle_options {
  my $print_help = 0;
  my $print_version = 0;
  my $result = GetOptions ("f|format=s" => \$format, "help|h" => \$print_help, "version|v" => \$print_version);
  if ($print_version) {
    print_version ();
    exit 0;
  }
  if ($print_help || (@ARGV == 0)) {
    print_help ();
    exit 0;
  }
}
handle_options ();



$divisions = 960;
$old_duration = $divisions;
$old_type = "";
$old_octave = 4;
$BEAM = 0;
@rhythmic_model = ();
$rhythmic_model_index = 0;

# Store all alterations already used in the current measure, so that e.g.
# xCC also detects the second note as a Cis! Needs to be reset at the
# beginning of a new bar.
%active_alterations = {};

($mday, $mon, $year) = (localtime()) [3..5];
$encoding_date = sprintf("%4d-%02d-%02d", $year + 1900, $mon+1, $mday);

$TIE = 0;

foreach $a (@ARGV) {
  $p = read_file($a);
  $toprint = "";
  if ($format eq "rism") {
    $p =~ s/\s*\=\=+\s*(.*?)\s*\=\=+\s*/$1/sg;
    $p =~ s/\s*included.*?-------------*\s*(.*?)\s*/$1/s;

    ($q, $r) = ($p, $p);
    if ($q !~ /^.*1\.1\.1.*$/gsx && $r =~ /^.*plain.*$/gsx) {
      print_error("$a contains 'plain', but not 1.1.1!\n");
    } else {
      if ($p =~ /^\s*([^\n]+)\n(.*?)\n((\d+\.\d+\.\d.*?plain.*?\n)+)(.*?)\n?([^\n]+)\n([^\n]+)\s*$/gs)    {
        my ($comp, $title, $incipits, $sonst, $libsig, $rismsig) = ($1, $2, $3, $5, $6, $7);

        $toprint .= "
COMPOSER:   $comp
TITLE:      $title
INCIPIT(S): $incipits
OTHER INFO: $sonst
LIB. SIGN.: $libsig
RISM SIGN.: $rismsig\n\n";
        parse_rism_incipits($incipits, $comp, $title, $sonst, $libsig, $rismsig);
      }
      else {
        if (index($p,"plain&easy") > -1) {
          print_error("Ignoring the following text:\n\n\n$p\n\n\n");
        }
      }
    }
  } else {
    # Just a plaine & easie snippet, without any further RISM fields
    if ($a eq "-") {
      $filename = "out.xml";
    } else {
      $filename = basename ($a, ".pae") . ".xml";
    }
    parse_pe ($filename, $p, "", "", "", "", "", "", "");
  }
}


##############################################################################
### RISM file parsing
##############################################################################

sub parse_rism_incipits {
  my ($incipits, $comp, $title, $sonst, $libsig, $rismsig) = @_;

  $toprint .= "parsing:   $incipits\n";

  while ($incipits =~ /^(\d+\.\d+\..+?)(\d+\.\d+\..*)$/gs) {
    my ($inc1) = $1;
    $incipits = $2;
    parse_rism_incipit($inc1, $comp, $title, $sonst, $libsig, $rismsig);
  }
  parse_rism_incipit($incipits, $comp, $title, $sonst, $libsig, $rismsig);
}

sub parse_rism_incipit {
  my ($pe, $comp, $title, $sonst, $libsig, $rismsig) = @_;

  if ($pe =~ /^\s*(\d+\.\d+\.\d)(\.|:)\s*(.*?)\nplain&easy:\s*(.*)$/gs) {
    my ($inr, $instr, $pecode) = ($1, $3, $4);

    my $filename="$rismsig-$inr.xml";
    $filename =~ s/RISM\s*A\/II\s*:?\s*//gs;

    foreach $_ ($rismsig,$title,$inr,$instr,$comp,$libsig,$sonst)
    {
        s/
//;
    }
    $toprint .= "
INCIPIT NO.:  $inr
INSTR.:       $instr\n";
    parse_pe ($filename, $pecode, $inr, $instr, $comp, $title, $sonst, $libsig, $rismsig);

  } else {
    print_error("could not parse $pe\n");
  }
}


##############################################################################
### pure Plaine & Easie data parsing
##############################################################################

sub parse_pe {
  my ($filename, $pe, $inr, $instr, $comp, $title, $sonst, $libsig, $rismsig) = @_;

  $pe =~ s/@ü/@0ü/gs; # make missing time signature explicit
  while ($pe =~ s/([^\-])(\d+)(\'|\,)(A|B|C|D|E|F|G)/$1$3$2$4/gs) {};  # octave first, then duration. Truly global.

  if ($pe =~ /^\s*(%([\w\-\+\d]+))?(@([\d\w\/ ]+))?\s*&?\s*(\$([^ü]*))?ü(.*)$/gs) {
    my ($clef, $timesig, $keysig, $rest) = ($2, $4, $6, $7);

    print "Writing $filename...\n";
    open(OUT, ">$filename");


    print OUT '<?xml version="1.0" encoding="iso-8859-1" standalone="no"?>
<!DOCTYPE score-partwise PUBLIC "-//Recordare//DTD MusicXML 2.0 Partwise//EN" "http://www.musicxml.org/dtds/partwise.dtd">
<score-partwise>
';
    print OUT "	<work>\n" if ($rismsig || $title);
    print OUT "		<work-number>$rismsig</work-number>\n" if ($rismsig);
    print OUT "		<work-title>$title</work-title>\n" if ($title);
    print OUT "	</work>\n" if ($rismsig || $title);
    print OUT "	<movement-number>$inr</movement-number>\n" if ($inr);
    print OUT "	<movement-title>$instr</movement-title>\n" if ($instr);
    print OUT "	<identification>\n";
    print OUT "		<creator type=\"composer\">$comp</creator>\n" if ($comp);
    print OUT '		<encoding>
			<software>pae2xml by R. Typke</software>
			<encoding-date>'.$encoding_date.'</encoding-date>
		</encoding>
';
    print OUT "		<source>$libsig</source>\n" if ($libsig);
    print OUT '	</identification>
	<part-list>
		<score-part id="P1">
			<part-name>'.$sonst.'</part-name>
		</score-part>
	</part-list>
	<part id="P1">
		<measure number="1">
			<attributes>
				<divisions>'.$divisions.'</divisions>
'.keysignature ($keysig)
.timesignature ($timesig)
.clef ($clef)
.'			</attributes>
';


    $toprint .= "
CLEF:         $clef
KEY SIG.:     $keysig
TIME SIG.:    $timesig
REST:         $rest\n";
    parse_notes($rest, $keysig);
  }
  else { print_error("could not parse $pe\n"); }
  print OUT "	</part>
</score-partwise>\n";
  close OUT;
}

# Repeat $1 by a count of $2
sub repeat {
  (my $e, my $count) = @_;
  my $res = "";
  for (my $i=1; $i <= $count; ++$i)
  {
    $res .= $e;
  }
  return $res;
}

sub parse_notes {
  my ($notes, $keysig) = @_;
  my $qq = 0; # in group of cue notes

  my $meas = 2;   # measure number
  my $mopen = 1;  # measure tag still open

  if ($notes =~ /^\s*(.*?)\s*$/) {
    $notes = $1;
  }

  $notes =~ s/!([^!]*)!(f*)/repeat($1, length($2)+1)/gse;  # write out repetitions
  while ( $notes =~ s/(:?\/+:?|^)([^\/:]*)(:?\/+:?)i(:?\/+:?)/$1$2$3$2$4/gs) {}; # replace whole-measure repeats (i notation)

  $notes =~ s/(\d+)\(([^;]+\))/\($1$2/gs;       # pull note lengths into fermatas or triplets
  $notes =~ s/(xx|x|bb|b|n)\(/\($1/gs;       # pull accidentals into tuplets or fermatas:
  $notes =~ s/(\d+)(xx|x|bb|b|n)(A|B|C|D|E|F|G)/$2$1$3/gs;  # accidentals first, then duration

  $notes =~ s/(\.|\d|\,|\')qq/qq$1/gs; # pull beginning mark of group of grace notes in front of corresponding notes
  $notes =~ s/(xx|x|bb|b|n)qq/qq$1/gs;  # qq first, then parts of notes
  $notes =~ s/([\.\d,'xbn])+(q|g)/$2$1/gs; # q and g first, then parts of notes
  $notes =~ s/(\{)(\d\.*(\d\.*)+)/$2$1/gs; # pull beaming inside a rhythmic model

  # Beam starts/endings are handled by the first/last note of the beam, since
  # we need to know for the first note the <beam>begin</beam> and for the
  # last note that the <beam>end</beam> tag should be used!
  # Thus, the RegExp for a note contains the beam start/end tags { and }
  while ($notes ne "") {
    if ($notes =~ /^(\'+|\,+)(.*)$/) {  # Octave marks
      ($oct, $notes) = ($1, $2);
      octave($oct);
    } elsif ($notes =~ /^qq(.*)$/) {  # Begin grace
      $notes = $1;
      $qq = 1;
    } elsif ($notes =~ /^r(.*)$/) {  # End grace
      $notes = $1;
      $qq = 0;
    } elsif ($notes =~ /^([0-9]\.?(?:\s?[0-9]+\.?)+)\s*(.*)$/) {  # Rhythmic model
      ($model, $notes) = ($1, $2);
      @rhythmic_model = parse_rhythmic_model ($1);
      $rhythmic_model_index = -1;
    } elsif ($notes =~ /^\=(\d*)(\/.*)$/) {  # multi-measure rests
      $measrest = $1;
      $notes = $2;
      if ($measrest eq '') {
        $measrest = 1;
      }
      $toprint .= "$measrest measures of rest.\n";
      if ($measrest > 0) {
        # Create a real multi-bar rest
        print OUT '			<attributes>
				<measure-style>
					<multiple-rest>'.$measrest.'</multiple-rest>
				</measure-style>
			</attributes>
';
      }
      # Now create the measures
      for $n (1..$measrest) {
	print OUT '			<note>
				<rest />
				<duration>'.($beats*$divisions*4/$beattype).'</duration>
			</note>
';
        if ($n < $measrest) {
          print OUT "		</measure>\n";
          reset_measure_attributes ();
          if ($notes ne "") {
            print OUT '		<measure number="'.$meas.'">
';
            $meas++;
          } else {
            $mopen = 0;
          }
        }
      }
    } elsif ($notes =~ /^({?(\^)?(g|q)?(\,|\')*(x|xx|b|bb|n)?\d*\.*(\-|A|B|C|D|E|F|G)t?\+?}?)(.*)$/) {  # a note
      ($note, $notes) = ($1,$7);
      parse_note($note, $keysig, "", "", $qq);
    } elsif ($notes =~ /^({?\((g|q)?{?(\,|\')*(x|xx|b|bb|n)?\d*\.*(\-|A|B|C|D|E|F|G)[t\+]*\)[t\+}]*)(.*)$/) {  # one note with a fermata
      ($note, $notes) = ($1,$6);
      parse_note($note, $keysig, "", "", $qq);
    } elsif ($notes =~ /^(\(({?(\^)?(g|q)?(\,|\')*(x|xx|b|bb|n)?\d*\.*(\-|A|B|C|D|E|F|G)t?\+?}?){3}\))(.*)$/) {  # a triplet
      ($triplet, $notes) = ($1,$8);
      $triplet =~ /^\(({?(\^)?(g|q)?(\,|\')*(x|xx|b|bb|n)?\d*\.*(\-|A|B|C|D|E|F|G)t?\+?}?)(.*)\)$/gs;
      ($note, $triplet) = ($1,$7);
      my $time_mod = "				<time-modification>
					<actual-notes>3</actual-notes>
					<normal-notes>2</normal-notes>
				</time-modification>\n";
      parse_note($note, $keysig, '<tuplet type="start"/>', $time_mod, $qq);
      $triplet =~ /^({?(g|q)?(\,|\')*(x|xx|b|bb|n)?\d*\.*(\-|A|B|C|D|E|F|G)t?\+?}?)(.*)$/gs;
      ($note, $triplet) = ($1,$6);
      parse_note($note, $keysig,  '', $time_mod, $qq);
      parse_note($triplet,  $keysig, '<tuplet type="stop"/>',  $time_mod, $qq);
    } elsif ($notes =~ /^((\d+)\(({?(\^)?(g|q)?(\,|\')*(x|xx|b|bb|n)?\d*\.*(\-|A|B|C|D|E|F|G)t?\+?}?)+\;(\d+)\))(.*)$/)  {  # an n-tuplet
      ($tuplet, $notes) = ($1,$10);
      $tuplet =~ /^(\d+)\(({?(\,|\')*(x|xx|b|bb|n)?(\d*\.*)(g|q)?(\-|A|B|C|D|E|F|G)t?\+?}?)(.*);(\d)\)$/gs;
      ($combdur, $note, $notedur, $tuplet, $numval) = ($1,$2,$5, $8,$9);
      my $act_notes = $numval;
      my $norm_notes = duration($combdur)/duration($notedur);
      my $time_mod = "				<time-modification>
					<actual-notes>$act_notes</actual-notes>
					<normal-notes>$norm_notes</normal-notes>
				</time-modification>\n";
      parse_note($note, $keysig, '<tuplet type="start"/>', $time_mod, $qq);
      while ($tuplet =~ /^({?(g|q)?(\,|\')*(x|xx|b|bb|n)?\d*\.*(\-|A|B|C|D|E|F|G)t?\+?\}?)([^}]+}?)$/gs) {
        ($note, $tuplet) = ($1,$6);
        parse_note($note, $keysig,  '', $time_mod, $qq);
      }
      parse_note($tuplet,  $keysig, '<tuplet type="stop"/>', $time_mod, $qq);
    } elsif ($notes =~ /^(%(\w(-|\+)\d))(.*)$/) {  # Clef change
      ($clef,$notes) = ($2,$4);
      print OUT "			<attributes>\n";
      print OUT clef ($clef);
      print OUT "			</attributes>\n";
    } elsif ($notes =~ /^@(c?\d+(\/\d+)?( \d+\/\d+)*|c\/?|o\.?)\s*(.*)$/) {  # time signatue change
      ($timesig,$notes) = ($1,$4);
      print OUT "			<attributes>\n";
      print OUT timesignature($timesig);
      print OUT "			</attributes>\n";
    } elsif ($notes =~ /^\$((b|x)[ABCDEFG]*)\s*(.*)$/) {  # key signature change
      ($keysig, $notes) = ($1, $3);
      print OUT "			<attributes>\n";
      print OUT keysignature ($keysig);
      print OUT "			</attributes>\n";
    } elsif ($notes =~ /^(:?\/+:?)(.*)$/) {  # Barline (and repeats)
      $barline = $1;
      $notes = $2;
      if ($barline =~ /^:\/\/:/) {
	print OUT '			<barline location="right">
				<bar-style>light-light</bar-style>
				<repeat direction="backward"/>
			</barline>
';
      } elsif ($barline =~ /^:\/\/$/ ) {
	  print OUT '			<barline location="right">
				<bar-style>light-heavy</bar-style>
				<repeat direction="backward"/>
			</barline>
';
      } elsif ($barline =~ /^\/\/$/) {
        $type = "light-light";
        # At the end of a piece use a light-heavy barline
        if ($notes eq "") {
          $type = "light-heavy";
        }
        print OUT "			<barline location=\"right\">
				<bar-style>$type</bar-style>
			</barline>
";
      }
      print OUT "		</measure>\n";
      reset_measure_attributes ();
      if ($notes ne "") {
	print OUT '		<measure number="'.$meas.'">
';
        if ($barline =~ /^\/\/:$/) {
          print OUT '			<barline location="left">
				<bar-style>heavy-light</bar-style>
				<repeat direction="forward"/>
			</barline>
';
        } elsif ($barline =~ /^:\/\/:$/) {
	  print OUT '			<barline location="left">
				<repeat direction="forward"/>
			</barline>
';
        }
        print OUT $clefattr;
        $meas++;
      } else {
        $mopen = 0;
      }
      $toprint .= "bar line\n";

    } elsif ($notes =~ /^\((\=)\)(.*)$/) {  # a bar of rest with a fermata
      ($rst, $notes) = ($1, $2);
      $toprint .= "rest: $rst\n";
      print OUT '			<note>
				<rest />
				<duration>'.($beats*$divisions*4/$beattype).'</duration>
				<notations>
					<fermata type="upright"/>
				</notations>
			</note>
';
    } elsif ($notes =~ s/(\d+\.*)\(({?(g|q)?(\,|\')*(x|xx|b|bb|n)?\d*\.*(\-|A|B|C|D|E|F|G)t?\+?}?)\)/\($1$2\)/gs) { # pull duration into fermata parentheses
      #      print "after replacement: $notes\n"; exit;
    } elsif ($notes =~ /^ +(.*)$/) {
      $notes = $1;
      print("Invalid space encountered in notes before $notes\n");
    } else {
      print_error("got stuck with $notes\n");
      $notes = "";
    }
  }
  if ($mopen) {
    print OUT "		</measure>\n";
    reset_measure_attributes ();
  }
}


sub parse_note {
  my($note, $keysig, $notation, $addition, $in_qq_group) = @_;

  my ($fermata) = (0);
  my ($actualnotes, $normalnotes) = (1,1);

  if ($addition =~ /^\s*<time-modification>\s*<actual-notes>\s*(\d+)\s*<\/actual-notes>\s*<normal-notes>\s*(\d+)\s*<\/normal-notes>\s*<\/time-modification>\s*$/) {
    ($actualnotes, $normalnotes) = ($1, $2);
  }

  if ($note =~ /^\((.*)\)(.*)$/) {
    $note = "$1$2";
    $fermata = 1;
  }

  $note =~ /^({)?(\^)?(g|q)?((\,|\')*)(x|xx|b|bb|n)?(\d*)(\.*)(\-|A|B|C|D|E|F|G)(t?)(\+?)(}?)$/;
  my ($beamstart, $chord, $gracecue, $oct, $acc, $dur, $dot, $pitch, $trill, $tie, $beamend) = ($1, $2, $3, $4, $6, $7, $8, $9, $10, $11, $12);

  print OUT '			<note>
';
  if ($gracecue eq "g") {
    print OUT '				<grace slash="yes" steal-time-following="33"/>
';
  }
  if ($gracecue eq "q" || $in_qq_group) {
    print OUT '				<grace/>
';
  }
  if ($pitch eq "-") {
    print OUT "				<rest />\n";
  } else {
    if ($chord eq "^") {
      print OUT "				<chord/>\n";
    }
    print OUT '				<pitch>
					<step>'.$pitch.'</step>
'.alter($pitch, $acc, $keysig)
.'					<octave>'.octave($oct).'</octave>
				</pitch>
';
  }
  # We are using a rhythmic model, extract the correct duration
  $this_duration = "";
  $this_head = "";
  if (($dur.$dot eq "") && scalar(@rhythmic_model)) {
    if ($chord ne "^") {
      $rhythmic_model_index = ($rhythmic_model_index +1 ) % scalar(@rhythmic_model);
    }
    $this_duration = $rhythmic_model[$rhythmic_model_index][0];
    $this_head = $rhythmic_model[$rhythmic_model_index][1];
  } elsif ($dur.$dot ne "" && scalar(@rhythmic_model)) {
    # The rhythmic model ends when a new new rhythmic value appears!
    @rhythmic_model = ();
  }
  if ($gracecue ne "g") {
    if (!$this_duration) {
      $this_duration = duration ($dur, $dot);
    }
    print OUT '				<duration>'.($this_duration*$normalnotes/$actualnotes).'</duration>
';
  }

  $tienotation = "";
  if ($tie eq "+") {
    if ($TIE) {
      $tienotation = "					<tied type=\"stop\"/>\n";
    }
    $tienotation .= "					<tied type=\"start\"/>\n";
    if (!$TIE) {
      $TIE = 1;
      print OUT '				<tie type="start"/>
';
    }
  } else {
    if ($TIE) {
      print OUT '				<tie type="stop"/>
';
      $tienotation = "					<tied type=\"stop\"/>\n";
      $TIE = 0;
    }
  }

  # Determine graphic notehead: acciaccaturas are always 8th, otherwise use duration
  if ($gracecue eq "g") {
    print OUT "					<type>eighth</type>\n";
  } else {
    if (!$this_head) {
      $this_head = notehead ($dur, $dot);
    }
    print OUT $this_head;
  }
  # If we have an explicit accidental on the note, print the <accidental> tag
  print OUT accidental_explicit ($acc);

  # addition is typically empty or a time-modification tag
  print OUT $addition;

  # print out beaming information if needed:
  if (($beamstart eq "{") && ($beamend eq "}")) {
    # Single-note beam means a hook
    print OUT "				<beam>forward hook</beam>\n";
  } elsif ($beamstart eq "{") {
    ++$BEAM;
    print OUT "				<beam number=\"$BEAM\">begin</beam>\n";
  } elsif (($BEAM > 0) && ($beamend eq "}")) {
    print OUT "				<beam number=\"$BEAM\">end</beam>\n";
    --$BEAM;
  } elsif ($BEAM > 0) {
    print OUT "				<beam number=\"$BEAM\">continue</beam>\n";
  }

  my $notationbracket = $fermata || $tienotation || ($trill eq "t") || ($notation ne "");
  if ($notationbracket) {
    print OUT "				<notations>\n";
  }
  if ($tienotation) {
    print OUT $tienotation;
  }
  if ($fermata) {
    print OUT "					<fermata type=\"upright\"/>\n";
  }
  if ($trill eq "t") {
    print OUT '					<ornaments>
						<trill-mark/>
					</ornaments>
';
  }
  if ($notation ne "") {
    print OUT "					$notation\n";
  }
  if ($notationbracket) {
    print OUT "				</notations>\n";
  }

  print OUT '			</note>
';

  $toprint .= "note: oct. $oct/acc. $acc/dur. $dur/dots $dot/grace,cue $gracecue/pitch $pitch\n";
}

sub reset_measure_attributes {
  %active_alterations = {};
  # TODO: reset all measure-only attributes, like manual accidentals
}

sub alter {
  my ($pitch, $acc, $keysig) = @_;

  my $alt = 0;
  # If we had the same pitch with explicit alteration already in the current
  # measure, that alteration goes on, unless the current note has an explicit
  # alteration
  if ($acc eq "") {
    $acc = $active_alterations{$pitch};
  } else {
    # Store the explicit alteration of the current pitch!
    $active_alterations{$pitch} = $acc;
  }

  if (index ($keysig,$pitch) > -1) {
    $keysig =~ /^(.).*$/gs;
    if ($1 eq 'x') {
      $alt = 1;
    } else {$alt = -1;}
  }

  my %acc_alt = ("n", 0, "b", -1, "bb", -2, "x", 1, "xx", 2);
  if ($acc_alt{$acc} ne "") {
    $alt = $acc_alt{$acc};
  }

  if ($alt != 0) {
    return "\t\t\t\t\t<alter>$alt</alter>\n";
  }
  return "";
}

sub accidental_explicit {
  my ($acc) = @_;
  my %accidentals = ("xx", "double-sharp", "x", "sharp", "n", "natural", "b", "flat", "bb", "flat-flat");
  my $this_acc = $accidentals{$acc};
  if ($this_acc) {
    return "\t\t\t\t<accidental>$this_acc</accidental>\n";
  } else {
    return "";
  }
}

sub raw_notehead {
  my ($duration, $dots) = @_;
  my %du=("0", "long", "9", "breve", "1", "whole", "2", "half", "4", "quarter",
          "8", "eighth", "6", "16th", "3", "32nd", "5", "64th", "7", "128th");
  if (($duration ne "") && $du{$duration}) {
    my $res = "				<type>$du{$duration}</type>\n";
    $res .= repeat ("				<dot/>\n", length ($dots));
    return $res;
  }
}
sub notehead {
  my ($duration, $dots) = @_;
  if ($duration.$dots ne "") {
    my $head = raw_notehead ($duration, $dots);
    $old_type = $head if $head;
  }
  return $old_type;
}

sub parse_rhythmic_model {
  (my $model) = @_;
  my @m = ();
  while ($model =~ s/^([0-9])\s*(\.?)\s*([0-9\.\s]*)$/$3/) {
    my ($this_dur, $this_dots) = ($1, $2);
    my $dur = raw_duration ($this_dur, $this_dots);
    my $notehead = raw_notehead ($this_dur, $this_dots);
    push @m, [$dur, $notehead];
  }
  return @m;
}


sub raw_duration {
  my ($duration, $dots) = @_;
  my %du=("1",4*$divisions,"2",2*$divisions,"4",$divisions,
          "8",$divisions/2,"6",$divisions/4,"3",$divisions/8,
          "5",$divisions/16,"7",$divisions/32,
          "9",$divisions*8,"0",$divisions*16); # breve/long
  my $res = $du{$duration};
  if ($res) {
    my $add = $res;
    while ($dots ne "") {
      $add /= 2;
      $res += $add;
      $dots =~ /^.(.*)$/gs;
      $dots = $1;
    }
  }
  return $res;
}
sub duration {
  my ($duration, $dots) = @_;
  if ($duration.$dots eq "7.") {
    print_error ("Neumic notation is not supported by MusicXML!");
  }

  if ($duration.$dots ne "") {
    $old_duration = raw_duration ($duration, $dots);
    if ($old_duration eq "") {
      print_error("strange duration '$duration'\n");
    }
  }
  return $old_duration;
}

sub octave {
  my ($octave) = @_;

  if ($octave ne "") {
    $octave =~ /^(.)(.*)$/gs;
    if ($1 eq ",") {
      $old_octave = 4 - length $octave;
    } else {
      $old_octave = 3 + length $octave;
    }
  }
  return $old_octave;
}

sub clef {
  my ($clef) = @_;
  my $clefoctave = '';
  if ($clef =~ /^(\w)(\-|\+)(\d)$/) {
    ($clefsign, $clefline) = ($1, $3);
    if ($2 =~ /^\+$/) {
      print "Warning: Mensural clefs are not supported by MusicXML, using modern clef (input: $clef)\n";
    }
    if ($clefsign eq 'g') {
      $clefsign = "G";
      $clefoctave = "					<clef-octave-change>-1</clef-octave-change>\n";
    }
  } else {
    ($clefsign, $clefline) = ("G", 2);
  }
  return '				<clef>
					<sign>'.$clefsign.'</sign>
					<line>'.$clefline.'</line>
'.$clefoctave.'				</clef>
';
}

sub keysignature {
  my ($keysig) = @_;

  # TODO: How is a change to C major represented? by "$ " or "$x " or "$b "?
  #       At the beginning, the $ part is left out, but mid-piece key changes
  #       need to way to clear all accidentals! We accept all three cases above!
  my %fif=("", 0, "x", 0, "b", 0, "xF", 1, "xFC", 2, "xFCG",3, "xFCGD",4, "xFCGDA",5, "xFCGDAE",6, "xFCGDAEB",7, "bB",-1, "bBE",-2, "bBEA",-3, "bBEAD",-4, "bBEADG",-5, "bBEADGC",-6, "bBEADGCF",-7);
  $keysig =~ s/(\s+)|&//gs;  # it is unclear what the & means, so we'll ignore it for now.
  $keysig =~ s/\[|\]//gs; # IGNORING brackets around a key sig.
  $fifths = $fif{$keysig};
  if ($fifths eq "") {
    $fifths = "0";
    print_error("Strange key signature '$keysig'.\n");
  }
  return '				<key>
					<fifths>'.$fifths.'</fifths>
				</key>
';
}


sub timesignature {
  my ($timesig) = @_;
  my $symbol = "";

  if ($timesig =~ /^(o(\.)?)$/) {
    if ($2 eq ".") {
      $timesig = "9/8";
    } else {
      $timesig = "3/4";
    }
    print "Mensural time signature \"$1\" not supported, using $timesig.\n";
  }
  if ($timesig =~ /^(\d+\/\d+)( \d+\/\d+)+$/ ) {
    print "Alternating time signature \"$timesig\" not supported by MusicXML, falling back to $1.\n";
    $timesig = $1;
  }
  if ($timesig =~ /^c((\d+)(\/(\d+))?)$/gs) {
    print "Time signature \"$timesig\" not supported by MusicXML, falling back to $1.\n";
    $timesig = "$1";  # TODO: it would be better to show the "C". Example: 451.023.814
  }

  # For missing timesignature, fall back to "c"
  if ($timesig eq "0" || $timesig eq "" || $timesig eq "c" ) {
    $symbol = "common";
    ($beats, $beattype) = (4,4);
  } elsif ($timesig =~ /^c\/$/gi) {
    $symbol = "cut";
    ($beats, $beattype) = (2,2);
  } elsif ($timesig =~ /^(\d+)\/(\d+)$/gs) {
    ($beats, $beattype) = ($1, $2);
  } elsif ($timesig =~/^(\d+)$/gs) {
    $symbol = "single-number";
    ($beats, $beattype) = ($1,2);
  } else {
    print_error("Time signature '$timesig' looks strange, falling back to 4/4.\n");
    ($beats, $beattype) = (4,4);
  }
  if ($symbol) {
    $symbol = " symbol=\"$symbol\"";
  }
  $timesig = "				<time$symbol>
					<beats>$beats</beats>
					<beat-type>$beattype</beat-type>
				</time>
";
  return $timesig;
}

sub print_error {
  my ($msg) = @_;

  print "\nAn error occurred; context:\n\n$toprint\n
Error: $msg\n";
}

sub read_file {
  my ($fn) = @_;
  my $res = "";
  if ($fn eq "-") {
      while (<STDIN>) { $res .= $_; } # read all lines
  } else {
    if (!(open FH, $fn)) {
      return "";
    }
    while (<FH>) { $res .= $_; } # read all lines
    close (FH);
  }
  return $res;
}
